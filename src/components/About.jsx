import React, { Component } from 'react';
import Rainbow from '../hoc/Rainbow.jsx'
const About = () => {
    return (
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos rerum velit quasi soluta maiores modi exercitationem inventore! Ducimus perspiciatis et, officiis laboriosam rerum fuga libero temporibus nihil dolore ab ad!</p>
        </div>
    )
}

//export default About;
export default Rainbow(About);