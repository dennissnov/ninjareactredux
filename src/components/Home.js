import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Pokeball from '../logo.svg'
import {connect} from 'react-redux'
class Home extends Component {
    render() {
        console.log(this.props)
        const {posts} = this.props;
        const postList = posts.length ? (
            posts.map(post => {
                return(
                    <div className="post card" key={post.id}>
                        <span className='image-container'>
                             <img src={Pokeball} className='img-home'/>
                        </span>
                        <div className="card-content">
                            <Link to={'/' + post.id}>
                                <div className="card-title">{post.title}</div>
                            </Link>
                            <p>{post.body}</p>
                        </div>
                    </div>
                )
               
            })
        ) : (
            <div className="center">No Post  yet</div>
        )
    return (

        
        <div className="container">
            <h4 className="center">Home</h4>
            {postList}
        </div>
        )
    }
}


const mapStateToProps = (state) =>{
    return {
       posts : state.posts 
    }
}
export default connect(mapStateToProps)(Home);//connect() HOC lalu wrapp si Home eta