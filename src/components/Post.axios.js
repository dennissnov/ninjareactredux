import React, { Component } from 'react';
import axios from 'axios'
import Pokeball from '../logo.svg'
//import action
class Post extends Component {
    state = {
        //id : null
        post : null
    }
    componentDidMount() {
        //console.log(this.props)
        let id = this.props.match.params.post_id
        axios.get('https://jsonplaceholder.typicode.com/posts/'+ id)
        .then(res =>{
            this.setState({
                post : res.data
            })
            console.log(res)
        })
        // this.setState({
        //     id : id
        // })
        
    }
    
    render(){
        const postDetail = this.state.post ? (
            <div className="post">
                <span className='image-container'>
                     <img src="https://placeimg.com/640/480/r"></img>
                </span>
                <h4 className="center">{this.state.post.title}</h4>
                <p>{this.state.post.body}</p>
            </div>
        ):(
            <div className="center">data ga ad</div>
        )
        return(
            <div className="container">
                {postDetail}
            </div>
        )
    }
}
export default Post