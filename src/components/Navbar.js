import React, { Component } from 'react';
import {Link, NavLink, withRouter} from 'react-router-dom'
const Navbar = (props) => {
    // setTimeout(()=>{
    //     props.history.push('/about')},2000
    // )
    return(
        // //FIRST METHOD
        // {/* <nav className="nav-wrapper red darken-3">
        //     <div className="contain">
        //         <a className="brand-logo">RWAR </a>
        //         <ul className="right">
        //             <li><a href="/">Home</a></li>
        //             <li><a href="/about">About</a></li>
        //             <li><a href="/contact">Contact</a></li>
        //         </ul>
        //     </div>
        // </nav> 
        // */}

    
        <nav className="nav-wrapper red darken-3">
            <div className="container">
                <a className="brand-logo">RWAR </a>
                <ul className="right">
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/about">About</NavLink></li>
                    <li><NavLink to="/contact">Contact</NavLink></li>
                </ul>
            </div>
        </nav>

    )

    
}

export default withRouter(Navbar)//supercharge component can get props