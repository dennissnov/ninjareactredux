import React, { Component } from 'react';
import axios from 'axios'
import Pokeball from '../logo.svg'
import {connect} from 'react-redux'
import {deletePost} from '../action/postActions'


class Post extends Component {
    handleClick = () => {
        console.log('delete')
        this.props.deletePost(this.props.post.id);
        this.props.history.push('/')
    }
    
    render(){
        console.log(this.props)
        const postDetail = this.props.post ? (
            <div className="post">
                <span className='image-container'>
                     <img src="https://placeimg.com/640/480/r"></img>
                </span>
                <h4 className="center">{this.props.post.title}</h4>
                <p>{this.props.post.body}</p>
                <div className="center">
                    <button className="btn grey" onClick={this.handleClick}>DELETE</button>
                </div>
            </div>
        ):(
            <div className="center">data ga ad</div>
        )
        return(
            <div className="container">
                {postDetail}
            </div>
        )
    }
}

//create function untuk mengambil props
// mengambil single prp
const mapStateToProps = (state, ownProps) =>{
    let id = ownProps.match.params.post_id;
    return {
        //sama kaya fungsi sebelumnya tapi kalau sebaris ini shorthand na
        post : state.posts.find(post => post.id === id)
    }
}
//ini kaya store.dispatch, m
const mapDispatchToProps = (dispatch) => {
    return {
        deletePost:(id) => {
            //MANUAL TYPE
           // dispatch({type:'DELETE_POST', id:id})
           dispatch(deletePost(id))
        }
    }

}

export default connect(mapStateToProps,mapDispatchToProps)(Post);