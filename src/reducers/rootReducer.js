const initState = {
    posts : [
        {id:'1',title:'ini title 1',body:'ini body 1'},
        {id:'2',title:'ini title 2',body:'ini body 2'},
        {id:'3',title:'ini title 3',body:'ini body 3'}
    ]
}


const rootReducer = (state = initState,action) => {
    console.log(action)
    if(action.type === 'DELETE_POST') {
        let newPosts = state.posts.filter(post =>{
            return action.id !== post.id
        })
        return {
            ...state,
            posts : newPosts
        }
    }
    return state;
}

export default rootReducer